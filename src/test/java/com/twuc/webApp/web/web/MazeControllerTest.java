package com.twuc.webApp.web.web;

import com.twuc.webApp.response.MazeExceptionResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class MazeControllerTest {
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    void should_not_get_maze_with_custom_400_response() {
        int statusCodeValue = restTemplate.
                getForEntity("/mazes/xxx", MazeExceptionResponse.class)
                .getStatusCodeValue();
        assertEquals(400, statusCodeValue);
    }
}
