package com.twuc.webApp.domain.mazeGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * 迷宫道路网格。该网格中的每一个点都代表了迷宫中的道路。
 */
public class Grid {
    private final GridCell[][] grid;
    private int rowCount;
    private int columnCount;

    /**
     * 创建一个迷宫网格。网格中的每一个道路点都相互不连通。
     *
     * @param rowCount 该迷宫的行数。
     * @param columnCount 该迷宫的列数。
     */
    public Grid(int rowCount, int columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        grid = prepareGrid(rowCount, columnCount);
        configCells();
    }

    private void configCells() {
        for (GridCell[] rows : grid) {
            for (GridCell cell : rows) {
                int cellRow = cell.getRow();
                int cellColumn = cell.getColumn();
                cell.setNeighbors(
                    this.getCell(cellRow - 1, cellColumn),
                    this.getCell(cellRow + 1, cellColumn),
                    this.getCell(cellRow, cellColumn + 1),
                    this.getCell(cellRow, cellColumn - 1));
            }
        }
    }

    private static GridCell[][] prepareGrid(int rows, int columns) {
        GridCell[][] theGrid = new GridCell[rows][];
        for (int rowIndex = 0; rowIndex < rows; ++rowIndex) {
            GridCell[] currentRow = new GridCell[columns];
            for (int columnIndex = 0; columnIndex < columns; ++columnIndex) {
                currentRow[columnIndex] = new GridCell(rowIndex, columnIndex);
            }

            theGrid[rowIndex] = currentRow;
        }

        return theGrid;
    }

    /**
     * 获得迷宫网格的行数。
     *
     * @return 行数。
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * 获得迷宫网格的列数。
     *
     * @return 列数。
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * 获得迷宫网格的所有行。
     *
     * @return 迷宫网格的所有行。每一行使用一个 {@link GridCell} 数组表示。
     */
    @SuppressWarnings("unused")
    public List<GridCell[]> getRows() {
        return Arrays.asList(grid);
    }

    /**
     * 获得迷宫中指定的路径点。
     *
     * @param rowIndex 该路径点所在的行（从 0 开始）
     * @param columnIndex 该路径点所在的列（从 0 开始）
     * @return 指定的路径点。如果该行或者该列不存在，则返回 {@code null}
     */
    public GridCell getCell(int rowIndex, int columnIndex) {
        if (rowIndex < 0 || rowIndex >= rowCount) return null;
        if (columnIndex < 0 || columnIndex >= columnCount) return null;
        return grid[rowIndex][columnIndex];
    }

    /**
     * 按照从从左到右从上到下的顺序依次返回迷宫中所有的路径点。
     *
     * @return 迷宫中所有的路径点。
     */
    public GridCell[] getCells() {
        return Arrays.stream(grid).flatMap(Arrays::stream).toArray(GridCell[]::new);
    }

    /**
     * 随机获得迷宫中的一个路径点。
     *
     * @return 迷宫中的一个路径点。
     */
    GridCell getRandomCell() {
        Random random = new Random();
        int rowIndex = random.nextInt(rowCount);
        int columnIndex = random.nextInt(grid[rowIndex].length);
        return this.getCell(rowIndex, columnIndex);
    }

    /**
     * 获得迷宫中路径点的个数。
     *
     * @return 迷宫中路径点的个数。
     */
    int size() {
        return rowCount * columnCount;
    }
}
