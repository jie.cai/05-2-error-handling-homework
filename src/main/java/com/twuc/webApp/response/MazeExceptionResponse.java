package com.twuc.webApp.response;

public class MazeExceptionResponse {
    private String message;

    public MazeExceptionResponse() {
    }

    public MazeExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static MazeExceptionResponse badRequest(String message) {
        return new MazeExceptionResponse(message);
    }
}
